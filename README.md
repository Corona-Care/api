# CoronaCare

In order to run this API you need to set two environment variables

* CONFIGURATION_SETUP represent the app configuration, options:
    * config.DevelopmentConfig
    * config.ProductionConfig
* SQLALCHEMY_DATABASE_URI should be a SQLAlchemy connection string.