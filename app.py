import json
import os

from flask import Flask, send_file
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
environment_configuration = os.environ['CONFIGURATION_SETUP']
app.config.from_object(environment_configuration)
db = SQLAlchemy(app)


def register_blueprints():
    #  Imports are here so we won't have import loops with SqlAlchemy

    from api.v1.endpoints.boxes import box_v1
    from api.v1.endpoints.csv_data import data_v1
    from api.v1.endpoints.patient import patient_v1
    from api.v1.endpoints.sample import sample_v1
    from api.v1.endpoints.session import sessions_v1

    app.register_blueprint(data_v1)
    app.register_blueprint(sample_v1)
    app.register_blueprint(patient_v1)
    app.register_blueprint(box_v1)
    app.register_blueprint(sessions_v1)


@app.route('/favicon.ico')
def favicon():
    return send_file(app.config.get("FAVICON_PATH"))


@app.route('/')
def homepage():
    return json.dumps({
        'Documentation': 'https://docs.google.com/document/d/1m13CQFrFhPGOxo8kndfq4AMo6d1_R9mZKfk3vPxxeyg/edit?usp=sharing',
        'Gitlab Group': 'https://gitlab.com/Corona-Care',
        'Backend Code': 'https://gitlab.com/Corona-Care/api',
        'Frontend Code': 'https://gitlab.com/Corona-Care/frontend',
        'Contributors':
            {
                '@Alon Barad': 'alon710@gmail.com',
                '@Aviv Sever': 'aviv.sever23@gmail.com',
                '@Dor Segal': 'segaldor0203@gmail.com',
                '@Guy Hirshorn': 'guydht1@gmail.com'
            }
    })


@app.errorhandler(400)
@app.errorhandler(404)
@app.errorhandler(500)
def http_error_handler(error):
    return {error.code: error.description}
