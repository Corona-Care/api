from flask_cors import CORS

from app import app, register_blueprints

CORS(app)
register_blueprints()

if __name__ == '__main__':
    app.run(host='0.0.0.0')
