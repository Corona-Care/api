import json

from flask import Blueprint, request, abort, Response, make_response
from pandas import DataFrame

from api.v1.resources.csv_data import CSVData
from api.v1.resources.fridge import Fridge
from api.v1.resources.sqlalchemy.location import Location

box_v1 = Blueprint('box_v1', __name__)


@box_v1.route('/api/v1/box/location/<string:box_id>', methods=['POST'])
def set_box_location(box_id):
    content = request.get_json()
    x, y, z = __extract_location(content)
    try:
        Location.set_location(box_id, x, y, z)
    except ValueError as e:
        response = Response(str(e), 400)
        abort(response)
    return json.dumps(dict(success=True)), 200


def __extract_location(json_content):
    x = json_content.get('x')
    y = json_content.get('y')
    z = json_content.get('z')
    return x, y, z


@box_v1.route('/api/v1/boxes', methods=['GET'])
def get_boxes():
    data = CSVData()
    fridge = Fridge()
    for patient, sessions in data.patients.items():
        for session in sessions.sessions:
            if session.samples:
                for sample_id in session.samples:
                    sample = session.samples[sample_id]
                    fridge.add_sample(sample)
    return json.dumps(fridge.boxes)


@box_v1.route('/api/v1/boxes/location', methods=['GET'])
def get_locations():
    locs_dict = Location.get_all_locations()
    df = DataFrame.from_dict(locs_dict)
    resp = make_response(df.to_csv(index=False))
    resp.headers["Content-Disposition"] = "attachment; filename=locations.csv"
    resp.headers["Content-Type"] = "text/csv"
    return resp
