import json

from flask import Blueprint, abort

from api.v1.resources.csv_data import CSVData

sessions_v1 = Blueprint('session_v1', __name__)


@sessions_v1.route('/api/v1/sessions/<string:study_num>', methods=['GET'])
def get_sessions(study_num):
    for _id, patient in CSVData().patients.items():
        if _id == study_num:
            return json.dumps(patient.sessions)
    abort(400)
