from flask import Blueprint, request, abort, current_app
import json

from api.v1.resources.csv_data import CSVData

data_v1 = Blueprint('data_v1', __name__)
CSV_FILE_NAME = 'sessions.csv'


@data_v1.route('/api/v1/csv', methods=['GET'])
def get_csv_data():
    return json.dumps(CSVData().patients)


def _is_valid_filename(filename: str):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() == 'csv'


@data_v1.route('/api/v1/csv', methods=['POST'])
def upload_csv():
    if not request.files.get('file'):
        abort(400)
    file = request.files['file']
    if file.filename == '':
        abort(400)
    if file and _is_valid_filename(file.filename):
        file.save(current_app.config['UPLOAD_FOLDER'].joinpath(CSV_FILE_NAME))
        return json.dumps(dict(success=True)), 200
    abort(400)
