from flask import Blueprint
from flask import abort

from api.v1.endpoints.boxes import get_boxes
from api.v1.resources.csv_data import CSVData
from consts.samples import COLORS
import json
sample_v1 = Blueprint('sample_v1', __name__)


@sample_v1.route('/api/v1/sample/<int:sample_id>', methods=['GET'])
def get_sample(sample_id):
    for patient in CSVData().patients.values():
        for session in patient.sessions:
            for sample, info in session.samples.items():
                if sample == sample_id:
                    return json.dumps(info)
    abort(400)


@sample_v1.route('/api/v1/samples/colors', methods=['GET'])
def get_samples_colors():
    return json.dumps(COLORS)


@sample_v1.route('/api/v1/sample/autocomplete', methods=['GET'])
def sample_autocomplete():
    autocomplete = {}
    for _id, box in json.loads(get_boxes()).items():
        for cell, sample in box['samples'].items():
            for k, v in sample.items():
                if autocomplete.get(k):
                    autocomplete[k].add(v)
                else:
                    autocomplete[k] = {v}
    return json.dumps({k: list(v) for k, v in autocomplete.items()})
