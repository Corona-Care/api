import json

from flask import Blueprint
from flask import abort

from api.v1.resources.csv_data import CSVData

patient_v1 = Blueprint('patient_v1', __name__)


@patient_v1.route('/api/v1/patient/<string:study_num>', methods=['GET'])
def get_patient(study_num):
    response = CSVData().patients.get(study_num)
    if response:
        return json.dumps(response)
    abort(404)
