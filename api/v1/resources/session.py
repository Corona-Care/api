from api.v1.resources.sample import Sample


class Session(dict):
    def __init__(self, study_num, visit_date, travel_ncov, contact_ncov, household_contact, presence_healthcare_ncov,
                 covid_confirm, disease_background, chronic_med, hospitalized, date_assessment, weight, height,
                 systolic_blood_pressure, diastolic_blood_pressure, temperature_celsius, heart_rate, visit_number,
                 blood_collect, blood_collect_date, bl_ser, samples=[], **kwargs):
        self.study_num = study_num
        self.visit_date = visit_date
        self.travel_ncov = travel_ncov
        self.contact_ncov = contact_ncov
        self.household_contact = household_contact
        self.presence_healthcare_ncov = presence_healthcare_ncov
        self.covid_confirm = covid_confirm
        self.disease_background = disease_background
        self.chronic_med = chronic_med
        self.hospitalized = hospitalized
        self.date_assessment = date_assessment
        self.height = height
        self.weight = weight
        self.systolic_blood_pressure = systolic_blood_pressure
        self.diastolic_blood_pressure = diastolic_blood_pressure
        self.temperature_celsius = temperature_celsius
        self.heart_rate = heart_rate
        self.visit_number = visit_number
        self.blood_collect = blood_collect
        self.blood_collect_date = blood_collect_date
        self.bl_ser = bl_ser
        self.samples = samples
        super().__init__(**self.__dict__)

    def add_sample(self, record):
        self.samples.append(Sample(**record))

    def __repr__(self):
        return self.__dict__.__str__()
