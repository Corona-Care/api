from typing import List

from app import db


class Location(db.Model):
    box_id = db.Column(db.String(50), primary_key=True, unique=True)
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    z = db.Column(db.Integer)

    def __init__(self, box_id: str):
        self.box_id = box_id
        self.x = None
        self.y = None
        self.z = None

    def __repr__(self):
        return f"{self.box_id} - ({self.x}, {self.y} , {self.z})"

    def location_json(self):
        return dict(x=self.x, y=self.y, z=self.z)

    def to_json(self):
        location_dict = dict(box_id=self.box_id)
        location_dict.update(self.location_json())
        return location_dict

    @classmethod
    def __get_location_by_box_id(cls, box_id: str):
        loc = cls.query.get(box_id)
        return loc

    @classmethod
    def get_location(cls, box_id: str):
        loc = cls.__get_location_by_box_id(box_id)
        if not loc:
            loc = cls(box_id)
            db.session.add(loc)
            db.session.commit()
            db.session.close()
        return loc

    @classmethod
    def set_location(cls, box_id: str, x: int, y: int, z: int):
        loc = cls.__get_location_by_box_id(box_id)
        if not loc:
            raise ValueError("Box doesn't exist")
        if cls.__is_location_exist(x, y, z):
            raise ValueError("Location already exist")
        loc.x = x
        loc.y = y
        loc.z = z
        db.session.commit()
        db.session.close()

    @classmethod
    def __validate_location(cls, loc, x: int, y: int, z: int):
        if not loc:
            raise ValueError("Box doesn't exist")
        if cls.__is_location_exist(x, y, z):
            raise ValueError("Location already exist")
        if cls.__is_numbers_positive([x, y, z]):
            raise ValueError("Location must be a positive number")

    @classmethod
    def __is_location_exist(cls, x: int, y: int, z: int):
        loc = cls.query.filter(cls.x == x, cls.y == y, cls.z == z).scalar()
        return loc is not None

    @classmethod
    def __is_numbers_positive(cls, numbers: List[int]):
        return all(i >= 0 for i in numbers)

    @classmethod
    def get_all_locations(cls):
        locs = cls.query.all()
        locs_dict = [loc.to_json() for loc in locs]
        db.session.close()
        return locs_dict
