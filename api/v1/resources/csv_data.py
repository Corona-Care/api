from pathlib import Path

from pandas import DataFrame
import pandas

from api.v1.resources.patient import Patient
from csv_parse import parse_record


class CSVData:
    _project_root_dir = Path().absolute()
    _csv_path = Path(_project_root_dir, 'static/sessions.csv')

    def __init__(self):
        self.session_data: DataFrame = self.load_csv()
        self.patients = self._get_patients()

    def load_csv(self) -> DataFrame:
        return pandas.read_csv(self._csv_path)

    def _get_patients(self):
        patients = {}
        for index, row in self.session_data.iterrows():
            parsed_record = parse_record(row)
            patient = Patient(**parsed_record)
            if patient.study_num not in patients:
                patients[patient.study_num] = patient
            patients[patient.study_num].add_session(parsed_record)
        return patients
