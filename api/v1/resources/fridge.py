from api.v1.resources.sample import Sample
from api.v1.resources.sqlalchemy.location import Location


class Fridge:
    def __init__(self):
        self.boxes = {}

    def add_sample(self, sample: Sample):
        self._add_box(sample)
        box_id = sample['box_id']
        self.boxes[box_id]['samples'][sample['cell']] = sample

    def _add_box(self, sample: Sample):
        box_id = sample['box_id']
        if not self.boxes.get(box_id):
            location = Location.get_location(box_id)
            self.boxes[box_id] = {'size': sample['box_size'], 'samples': {}, 'location': location.location_json()}
