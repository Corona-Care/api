from typing import List

from api.v1.resources.session import Session


class Patient(dict):

    def __init__(self, study_num, religions, ethnics, place_living, healthcare_personnel, smoking_years, **kwargs):
        self.study_num = study_num
        self.religions = religions
        self.ethnic = ethnics
        self.place_living = place_living,
        self.healthcare_personnel = healthcare_personnel
        self.smoking_years = smoking_years
        self.sessions: List[Session] = []
        super().__init__(**self.__dict__)

    def add_session(self, record):
        self.sessions.append(Session(**record))
