import os
from pathlib import Path


class Config(object):
    TESTING = False
    UPLOAD_FOLDER = Path('static')
    FAVICON_PATH = Path(UPLOAD_FOLDER, "favicon.ico")
    APP = 'app.py'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")


class DevelopmentConfig(Config):
    ENV = "development"
    DEBUG = True
    PORT = 5000


class ProductionConfig(Config):
    DEBUG = False
    PORT = 80
