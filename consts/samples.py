SERUM = "serum"
PBMC = "pbmc"
PLASMA = "plasma"
WHOLE_BLOOD = "whole_blood"
PAX = "pax"
URINE = "urine"
STOOL = "stool"
SAMPLES = 'samples'

COLORS = {
    SERUM: 'purple',
    PBMC: 'blue',
    PLASMA: 'green',
    WHOLE_BLOOD: 'red',
    PAX: 'orange',
    URINE: 'yellow',
    STOOL: 'brown'
}
