from typing import Dict

from pandas import Series

from api.v1.resources.sample import Sample
from consts import samples

RELIGIONS = {1: 'Jewish', 2: 'Muslim', 3: 'Christian', 4: 'Other'}
ETHNICS = {1: 'Ashkenaz', 2: 'North Africa', 3: 'Iraq', 4: 'Iran', 5: 'Ethiopia',
           6: 'Caucasus', 7: 'Bukhara', 8: 'Kurdistan', 9: 'Yemen', 10: 'Arab', 11: 'Other'}
DISEASES = {1: 'IHD', 2: 'CHF', 3: 'Chronic lung disease / COPD', 4: 'Asthma', 5: 'CKD',
            6: 'Mod-severe liver disease', 7: 'Mild liver disease', 8: 'Chronic neurological disorder',
            9: 'PVD', 10: 'PUD', 11: 'Hematologic malignancy', 12: 'Metastatic solid tumor', 13: 'AIDS',
            14: 'Diabetes with complications', 15: 'Diabetes without complications',
            16: 'Rheumatologic disorder', 17: 'Dementia', 18: 'Hypertension', 19: 'Morbid Obesity',
            20: 'Smoker', 21: 'Other'}

LIVING_PLACES = {1: 'Home', 2: 'Nursing Home', 3: 'Other'}
BOX_SIZES = dict(serum=100, pbmc=100, plasma=100, whole_blood=100, pax=36, urine=42, stool=42)


def parse_checkbox(mapper: Dict[int, str], choices: Series):
    results = []
    for index, choice in enumerate(choices, start=1):
        if choice == 1:
            results.append(mapper.get(index, None))
    return results


def parse_bool(yesno: str):
    return True if yesno != 0 and not is_nan(yesno) else False


def parse_optional(condition, col_range: Series):
    if parse_bool(condition):
        return [parse_text(value) for value in col_range]


def parse_text(value):
    if not is_nan(value):
        if type(value) == float:
            return int(value)
        return str(value)
    return None


def parse_date(date):
    return str(date) if not is_nan(date) else None


def parse_radio(mapping: dict, index):
    return mapping.get(index)


def is_nan(value):
    return value != value


def parse_sample(_type, study_num, barcode, box, cell, barcod_remov_yn, barcod_shipped_txt, barcod_shipped_dt):
    params = dict(
        barcode=str(parse_text(barcode)),
        box=parse_text(box),
        cell=parse_text(cell),
        is_removed=parse_bool(barcod_remov_yn),
        shipped_txt=parse_text(barcod_shipped_txt),
        shipped_dt=parse_date(barcod_shipped_dt),
        type=_type,
        study_num=study_num,
        box_size=BOX_SIZES[_type],
        box_id=_type + str(int(box))
    )
    if params:
        return Sample(**params)


CONDITION_PARSE_MAP = {
    66: ('chronic_med', (67, 75)),
    60: ('disease_background', (61, 66)),
}
SINGLE_PARSE_MAP = {
    0: ('study_num', parse_text),
    2: ('enroll_date', parse_date),
    8: ('dob', parse_date),
    9: ('scrn_age', parse_text),
    10: ('residence_city', parse_text),
    34: ('healthcare_personnel', parse_bool),
    59: ('smoking_years', parse_text),
    77: ('visit_date', parse_date),
    30: ('travel_ncov', parse_bool),
    31: ('contact_ncov', parse_bool),
    32: ('household_contact', parse_bool),
    33: ('presence_healthcare_ncov', parse_bool),
    35: ('covid_confirm', parse_bool),
    78: ('hospitalized', parse_bool),
    81: ('date_assessment', parse_date),
    82: ('height', parse_text),
    83: ('weight', parse_text),
    84: ('systolic_blood_pressure', parse_text),
    85: ('diastolic_blood_pressure', parse_text),
    86: ('temperature_celsius', parse_text),
    87: ('heart_rate', parse_text),
    90: ('visit_number', parse_text),
    92: ('blood_collect', parse_bool),
    93: ('blood_collect_date', parse_date),
    94: ('bl_ser', parse_bool),
}
CHECKBOX_PARSE_MAP = {
    38: ('diseases', DISEASES, (38, 59)),
    11: ('religions', RELIGIONS, (11, 15)),
    15: ('ethnics', ETHNICS, (15, 27)),
}
RADIO_PARSE_MAP = {
    28: ('place_living', LIVING_PLACES),
}

SAMPLE_PARSE_MAPPING = {
    96: samples.SERUM,
    102: samples.SERUM,
    108: samples.SERUM,
    114: samples.SERUM,
    120: samples.SERUM,
    126: samples.SERUM,
    132: samples.SERUM,
    138: samples.SERUM,
    144: samples.SERUM,
    150: samples.SERUM,
    156: samples.SERUM,
    162: samples.SERUM,
    168: samples.SERUM,
    174: samples.SERUM,
    180: samples.SERUM,
    186: samples.SERUM,
    192: samples.SERUM,
    198: samples.SERUM,
    204: samples.SERUM,
    210: samples.SERUM,
    218: samples.PBMC,
    224: samples.PBMC,
    230: samples.PBMC,
    238: samples.PLASMA,
    244: samples.PLASMA,
    250: samples.PLASMA,
    256: samples.PLASMA,
    264: samples.WHOLE_BLOOD,
    270: samples.WHOLE_BLOOD,
    276: samples.WHOLE_BLOOD,
    282: samples.WHOLE_BLOOD,
    289: samples.PAX,
    298: samples.URINE,
    304: samples.URINE,
    310: samples.URINE,
    316: samples.URINE,
    325: samples.STOOL,
    331: samples.STOOL,
    337: samples.STOOL
}


def parse_record(row: Series):
    parsed_record = dict()
    parsed_record[samples.SAMPLES] = {}
    for index, value in enumerate(row):
        try:
            study_num = parse_text(row[0])
            if index in SINGLE_PARSE_MAP:
                field_name, func = SINGLE_PARSE_MAP[index]
                parsed_record[field_name] = func(value)
            elif index in CHECKBOX_PARSE_MAP:
                field_name, mapping, col_range = CHECKBOX_PARSE_MAP[index]
                parsed_record[field_name] = parse_checkbox(mapping, row[col_range[0]:col_range[1]])
            elif index in RADIO_PARSE_MAP:
                field_name, mapping = RADIO_PARSE_MAP[index]
                parsed_record[field_name] = parse_radio(mapping, 0 if is_nan(value) else int(value))
            elif index in CONDITION_PARSE_MAP:
                field_name, col_range = CONDITION_PARSE_MAP[index]
                parsed_record[field_name] = parse_optional(index, row[col_range[0]:col_range[1]])
            elif index in SAMPLE_PARSE_MAPPING:
                _type = SAMPLE_PARSE_MAPPING[index]
                field_name = parse_text(value)
                if field_name:
                    parsed_record[samples.SAMPLES][field_name] = parse_sample(_type, study_num, *row[index:index + 6])
        except Exception as e:
            raise e
    return parsed_record
